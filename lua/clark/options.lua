local options = {
    tabstop = 4,
    softtabstop = 4,
    expandtab = true,
    shiftwidth = 4,

    relativenumber = true,
    number = true,

    wrap = false,

    cul = true,
    clipboard = 'unnamedplus',
    completeopt = { "menuone", "noselect" },
    conceallevel = 0,
    fileencoding = 'utf-8',
    hidden = true,
    hlsearch = true,
    ignorecase = true,
    mouse = "",
    pumheight = 10,
    showmode = false,
    showtabline = 2,
    smartindent = true,
    termguicolors = true,
    swapfile = false,
    updatetime = 300,
    timeoutlen = 100,
    writebackup = false,
    scrolloff = 8,
    guicursor = "",
    sidescrolloff = 8,
}

for k,v in pairs(options) do
    vim.opt[k] = v
end
