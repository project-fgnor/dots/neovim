local ensure_packer = function()
    local fn = vim.fn
    local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
    if fn.empty(fn.glob(install_path)) > 0 then
        fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
        vim.cmd [[packadd packer.nvim]]
        return true
    end
    return false end

    vim.cmd([[
    augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
    augroup end
    ]])

    local packer_bootstrap = ensure_packer()

    return require('packer').startup(function(use)
        -- Let packer manage itself
        use 'wbthomason/packer.nvim'

        use({ "catppuccin/nvim", as = "catppuccin", config = function()
            vim.cmd.colorscheme "catppuccin"
            require("catppuccin").setup({
                flavour = "mocha",
                transparent_background = true,
            })
        end})

        use({ "akinsho/bufferline.nvim", config = function() require('clark.settings.bufferline') end })

        use({
            "williamboman/mason.nvim",
            "williamboman/mason-lspconfig.nvim",
        })

        use({ 'jose-elias-alvarez/null-ls.nvim', config = function() require('clark.settings.nullls') end })
        use({ 'MunifTanjim/prettier.nvim' })

        use({ 'hrsh7th/nvim-cmp', config = function() require('clark.settings.cmp') end, event = {'BufRead'} })
        use({ 'hrsh7th/cmp-nvim-lsp' })
        use({ 'L3MON4D3/LuaSnip' })
        use({ 'saadparwaiz1/cmp_luasnip', after = "nvim-cmp" })

        use({ 'nvim-treesitter/nvim-treesitter', event = {'BufRead'}, config = function() require('clark.settings.treesitter') end, })

        use({ "neovim/nvim-lspconfig", config = function() require('clark.settings.lspconfig') end, })

        use({ 'windwp/nvim-autopairs', config = function() require('clark.settings.aupairs') end, })


        use {
            "nvim-neo-tree/neo-tree.nvim",
            branch = "v2.x",
            config = function()
                require("clark.settings.neotree")
            end,
            requires = { 
                "nvim-lua/plenary.nvim",
                "nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
                "MunifTanjim/nui.nvim",
            }
        }

        if packer_bootstrap then
            require('packer').sync()
        end
    end)
