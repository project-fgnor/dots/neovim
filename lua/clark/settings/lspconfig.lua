local capabilities = require("cmp_nvim_lsp").default_capabilities()

require("mason").setup()
require("mason-lspconfig").setup({
    ensure_installed = { "lua_ls" },
    capabilities = capabilities
})

require("lspconfig").lua_ls.setup {}
require("lspconfig").tsserver.setup {}
