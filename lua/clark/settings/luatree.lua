vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

vim.opt.termguicolors = true

require("nvim-tree").setup()

local function open_nvim_tree()

  require("nvim-tree.api").tree.toggle()
end
vim.api.nvim_create_autocmd({ "VimEnter" }, { callback = open_nvim_tree })
